//
// Created by explorentis on 05.05.2022.
//

#include "Control.h"
#include "gamers/Human.h"
#include "gamers/Static_X_3_3_Draw.h"
#include "utils.h"

Control::Control():
    xgamer(new Static_X_3_3_Draw()),
    ogamer(new Human()),
    lastTurn()
{
}

Turn Control::getLastTurn() const
{
    return this->lastTurn;
}

void Control::step(Board &boardState, PlayerMark currentPlayer)
{
    do
    {
        if (currentPlayer == PlayerMark::x)
        {
            this->lastTurn = this->xgamer->step(boardState);
        } else {
            this->lastTurn = this->ogamer->step(boardState);
        }
    }
    while (this->isTurnWrong(boardState));
}

bool Control::isTurnWrong(Board &board)
{
    return utils::getPlayerAtPosition(board, this->lastTurn.x, this->lastTurn.y) != PlayerMark::none;
}