//
// Created by explorentis on 05.05.2022.
//

#ifndef XO_CONTROL_H
#define XO_CONTROL_H

#include "Turn.h"
#include "gamers/Gamer.h"
#include "typedefs.h"

class Control {
    Turn lastTurn;
    Gamer *xgamer, *ogamer;
    bool isTurnWrong(Board &board);
public:
    Control();
    void step(Board &boardState, PlayerMark currentPlayer);
    [[nodiscard]] Turn getLastTurn() const;
};


#endif //XO_CONTROL_H
