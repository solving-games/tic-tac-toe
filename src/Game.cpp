//
// Created by explorentis on 05.05.2022.
//

#include "Game.h"
#include <stdexcept>

Game::Game(BoardPos boardSize, BoardPos minimalRow):
        model{boardSize, minimalRow},
        control(),
        view()
{};

void Game::play()
{
    while (not this->model.isGameFinished())
    {
        this->step();
    }
    this->view.showWinner(this->model.getWinner());
}

void Game::step()
{
    this->view.step(this->model.getBoard());
    this->control.step(this->model.getBoard(), this->model.getCurrentPlayer());
    this->model.step(this->control.getLastTurn());
}
