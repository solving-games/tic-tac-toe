//
// Created by explorentis on 05.05.2022.
//

#ifndef XO_GAME_H
#define XO_GAME_H

#include "Model.h"
#include "Control.h"
#include "View.h"

class Game {
public:
    Game(BoardPos boardSize=3, BoardPos minimalRow=3);
    void play();
private:
    Model model;
    Control control;
    View view;

    void step();
};


#endif //XO_GAME_H
