//
// Created by explorentis on 05.05.2022.
//

#include <stdexcept>
#include "Model.h"
#include "utils.h"

Model::Model(BoardPos boardSize, BoardPos minimalRow):
    isFinished(false),
    winner(PlayerMark::none),
    nowTurn(PlayerMark::x),
    minimalRowToWin(minimalRow),
    turnNumber(0),
    maxTurnNumber(boardSize * boardSize)
{
    std::list<PlayerMark> row;
    for (unsigned short int i = 0; i != boardSize; ++i)
    {
        for (unsigned short int j = 0; j != boardSize; ++j)
        {
            row.emplace_back(PlayerMark::none);
        }
        this->board.emplace_back(row);
        row.clear();
    }
}

bool Model::makeTurn(BoardPos x, BoardPos y)
{
    auto row = this->board.begin();
    std::advance(row, y);
    auto cell = row->begin();
    std::advance(cell, x);
    if (*cell == PlayerMark::none)
    {
        *cell = this->nowTurn;
        return true;
    }
    return false;
}

void Model::step(const Turn& turn)
{
    if (this->turnNumber == this->maxTurnNumber - 1)
    {
        this->isFinished = true;
    }
    this->makeTurn(turn.x, turn.y);

    this->checkFinish();
    this->nowTurn = (this->nowTurn == PlayerMark::x ? PlayerMark::o : PlayerMark::x);
    this->turnNumber++;
}

bool Model::isGameFinished() const
{
    return this->isFinished;
}

PlayerMark Model::getWinner() const
{
    return this->winner;
}

[[nodiscard]] Board& Model::getBoard()
{
    return this->board;
}

[[nodiscard]] PlayerMark Model::getCurrentPlayer()
{
    return this->nowTurn;
}

void Model::checkFinish()
{
    BoardPos columnIndex = 0;
    BoardPos rowIndex = 0;
    for (auto &row: this->board)
    {
        columnIndex = 0;
        for (auto &cell: row)
        {
            if (cell == this->getCurrentPlayer())
            {
                if (this->checkE(columnIndex, rowIndex)
                or this->checkSE(columnIndex, rowIndex)
                or this->checkS(columnIndex, rowIndex)
                or this->checkSW(columnIndex, rowIndex))
                {
                    this->isFinished = true;
                    this->winner = this->getCurrentPlayer();
                }
            }
            columnIndex++;
        }
        rowIndex++;
    }
}


bool Model::checkE(BoardPos columnIndex, BoardPos rowIndex)
{
    for (int i = 0; i != this->minimalRowToWin; i++)
    {
        if (utils::getPlayerAtPosition(this->board, columnIndex + i, rowIndex) != this->getCurrentPlayer())
            return false;
    }
    return true;
}

bool Model::checkSE(BoardPos columnIndex, BoardPos rowIndex)
{
    for (int i = 0; i != this->minimalRowToWin; i++)
    {
        if (utils::getPlayerAtPosition(this->board, columnIndex + i, rowIndex + i) != this->getCurrentPlayer())
            return false;
    }
    return true;
}

bool Model::checkS(BoardPos columnIndex, BoardPos rowIndex)
{
    for (int i = 0; i != this->minimalRowToWin; i++)
    {
        if (utils::getPlayerAtPosition(this->board, columnIndex, rowIndex + i) != this->getCurrentPlayer())
        return false;
    }
    return true;

}

bool Model::checkSW(BoardPos columnIndex, BoardPos rowIndex)
{
    for (int i = 0; i != this->minimalRowToWin; i++)
    {
        if (utils::getPlayerAtPosition(this->board, columnIndex - i, rowIndex + i) != this->getCurrentPlayer())
        return false;
    }
    return true;
}
