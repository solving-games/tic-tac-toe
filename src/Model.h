//
// Created by explorentis on 05.05.2022.
//

#ifndef XO_MODEL_H
#define XO_MODEL_H

#include "enums.h"
#include "typedefs.h"
#include "Turn.h"

class Model {
public:
    explicit Model(BoardPos boardSize=3, BoardPos minimalRow=3);
    void step(const Turn& turn);
    [[nodiscard]] bool isGameFinished() const;
    [[nodiscard]] PlayerMark getWinner() const;
    [[nodiscard]] Board& getBoard();
    [[nodiscard]] PlayerMark getCurrentPlayer();
private:
    bool isFinished;
    PlayerMark winner;
    PlayerMark nowTurn;
    Board board;
    BoardPos minimalRowToWin;
    unsigned int turnNumber;
    unsigned int maxTurnNumber;

    bool makeTurn(BoardPos x, BoardPos y);
    void checkFinish();

    bool checkE(BoardPos row, BoardPos col);
    bool checkSE(BoardPos row, BoardPos col);
    bool checkS(BoardPos row, BoardPos col);
    bool checkSW(BoardPos row, BoardPos col);
};


#endif //XO_MODEL_H
