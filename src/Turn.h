//
// Created by explorentis on 07.05.2022.
//

#ifndef XO_TURN_H
#define XO_TURN_H

#include "enums.h"

class Turn {
public:
    Turn() = default;
    Turn(int x, int y);
    int x, y;
};


#endif //XO_TURN_H
