//
// Created by explorentis on 05.05.2022.
//

#include <stdexcept>
#include <cstdio>
#include "View.h"
#include "consts.h"

void View::step(const Board& board)
{
    int row_num = 0;
    printf("┌");
    for (int i = 0; i != board.front().size() - 1; i ++)
    {
        printf("───┬");
    }
    printf("───┐\n");
    for (auto &row: board)
    {
        for (auto cell: row)
        {
            printf("│ %c ", View::renderPlayer(cell));
        }
        printf("│\n");
        if (row_num != board.front().size() - 1)
        {
            printf("├");
            for (int i = 0; i != board.front().size() - 1; i ++)
            {
                printf("───┼");
            }
            printf("───┤\n");
        }
        row_num++;
    }
    printf("└");
    for (int i = 0; i != board.front().size() - 1; i ++)
    {
        printf("───┴");
    }
    printf("───┘\n");

}

char View::renderPlayer(const PlayerMark& player)
{
    return playerGraphModel[player];
}

void View::showWinner(const PlayerMark &winner)
{
    if (winner == PlayerMark::none)
    {
        printf("Ничья!");
        return;
    }
    printf("Победил %c\n", View::renderPlayer(winner));
}