//
// Created by explorentis on 05.05.2022.
//

#ifndef XO_VIEW_H
#define XO_VIEW_H

#include <list>
#include "enums.h"
#include "typedefs.h"

class View {
public:
    static void step(const Board& board);
    static char renderPlayer(const PlayerMark& player);
    static void showWinner(const PlayerMark& winner);
};


#endif //XO_VIEW_H
