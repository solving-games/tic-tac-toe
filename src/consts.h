//
// Created by explorentis on 09.05.2022.
//

#ifndef XO_CONSTS_H
#define XO_CONSTS_H

#include <map>
#include <enums.h>

std::map<PlayerMark, char> playerGraphModel = {
        {PlayerMark::none, ' '},
        {PlayerMark::x,    'x'},
        {PlayerMark::o,    'o'}
};


#endif //XO_CONSTS_H
