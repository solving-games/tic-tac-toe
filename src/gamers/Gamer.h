//
// Created by explorentis on 13.05.2022.
//

#ifndef XO_GAMER_H
#define XO_GAMER_H

#include "typedefs.h"
#include "Turn.h"

class Gamer
{
public:
    virtual Turn step(Board& board) = 0;
};


#endif //XO_GAMER_H
