//
// Created by explorentis on 13.05.2022.
//

#include <stdexcept>
#include "Human.h"
#include "utils.h"

Turn Human::step(Board& board)
{
    int X = -1, Y = -1;
    while ((X < 0)
        or (X >= board.size())
        or (Y < 0)
        or (Y >= board.front().size())
        or (utils::getPlayerAtPosition(board, X, Y) != PlayerMark::none))
    {
        printf("Введите место, куда хотите поставить Ваш символ в формате X Y: ");
        scanf("%d %d", &X, &Y);
    }
    return {X, Y};
};