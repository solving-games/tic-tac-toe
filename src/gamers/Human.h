//
// Created by explorentis on 13.05.2022.
//

#ifndef XO_HUMAN_H
#define XO_HUMAN_H

#include "Gamer.h"

class Human: public Gamer
{
    Turn step(Board& board) override;

};


#endif //XO_HUMAN_H
