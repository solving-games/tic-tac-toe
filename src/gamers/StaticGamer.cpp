//
// Created by explorentis on 17.05.2022.
//

#include "StaticGamer.h"
#include "utils.h"

Turn StaticGamer::step(Board& board)
{
    if (this->turns.empty())
    {
        for (int y = 0; y != board.size(); y++)
        {
            for (int x = 0; x != board.front().size(); x++)
            {
                if (utils::getPlayerAtPosition(board, x, y) == PlayerMark::none)
                {
                    return {x, y};
                }
            }
        }
    }
    Turn turn = this->turns.front();
    this->turns.pop_front();
    return turn;
}