//
// Created by explorentis on 17.05.2022.
//

#ifndef XO_STATICGAMER_H
#define XO_STATICGAMER_H

#include <list>
#include "Turn.h"
#include "Gamer.h"

class StaticGamer: public Gamer{
public:
    Turn step(Board& board) override;

protected:
    std::list<Turn> turns;
};


#endif //XO_STATICGAMER_H
