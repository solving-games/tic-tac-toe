//
// Created by explorentis on 17.05.2022.
//

#include "Static_X_3_3_Draw.h"

Static_X_3_3_Draw::Static_X_3_3_Draw()
{
    this->turns = {
            {0, 0},
            {0, 2},
            {1, 1},
            {1, 0},
            {2, 1}
    };
}