//
// Created by explorentis on 06.05.2022.
//

#ifndef XO_TYPEDEFS_H
#define XO_TYPEDEFS_H

#include <list>
#include "enums.h"

typedef unsigned short int BoardPos;
typedef std::list<std::list<PlayerMark> > Board;

#endif //XO_TYPEDEFS_H
