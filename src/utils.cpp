//
// Created by explorentis on 16.05.2022.
//

#include "utils.h"

PlayerMark utils::getPlayerAtPosition(Board& board, BoardPos columnIndex, BoardPos rowIndex)
{
    if (columnIndex < 0
        or columnIndex >= board.size()
        or rowIndex < 0
        or rowIndex >= board.front().size())
        return PlayerMark::wrong;
    Board::iterator itrow = board.begin();
    std::advance(itrow, rowIndex);
    std::list<PlayerMark>::iterator itcol = itrow->begin();
    std::advance(itcol, columnIndex);
    return *itcol;
}
