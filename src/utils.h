//
// Created by explorentis on 16.05.2022.
//

#ifndef XO_UTILS_H
#define XO_UTILS_H

#include "typedefs.h"

namespace utils
{
    PlayerMark getPlayerAtPosition(Board& board, BoardPos columnIndex, BoardPos rowIndex);
}


#endif //XO_UTILS_H
